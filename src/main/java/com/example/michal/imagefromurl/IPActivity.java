package com.example.michal.imagefromurl;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class IPActivity extends AppCompatActivity {

    EditText EditIpAdress;
    EditText EditPort;
    EditText EditMes;
    Button buttonConnect;

    UdpClientThread udpClientThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_ip);
        EditIpAdress = (EditText) findViewById(R.id.IPadress);
        EditIpAdress.setText(Model.dstAddress);
        EditPort = (EditText) findViewById(R.id.UDPPort);
        EditPort.setText(Integer.toString(Model.dstPort));
        EditMes = (EditText) findViewById(R.id.InputMes);
        buttonConnect = (Button) findViewById(R.id.button);

        buttonConnect.setOnClickListener(buttonConnectOnClickListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        udpClientThread = null;
    }

    View.OnClickListener buttonConnectOnClickListener =
            new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Model.dstAddress = EditIpAdress.getText().toString();
                    Model.dstPort = Integer.parseInt(EditPort.getText().toString());
                    udpClientThread = new UdpClientThread(EditMes.getText().toString());
                    udpClientThread.start();

                }
            };
}
