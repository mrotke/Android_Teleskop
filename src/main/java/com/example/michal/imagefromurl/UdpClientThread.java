package com.example.michal.imagefromurl;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * Created by michal on 7/13/18.
 */

public class UdpClientThread extends Thread {



    DatagramSocket socket;
    InetAddress address;
    String message;

    public UdpClientThread(String mes) {
        super();
        message = mes;
    }

    @Override
    public void run() {
        try {
            socket = new DatagramSocket();
            address = InetAddress.getByName(Model.dstAddress);

            int msg_length=message.length();
            byte[] mes = message.getBytes();

            DatagramPacket p = new DatagramPacket(mes, msg_length,address,Model.dstPort);
            socket.send(p);


        } catch (UnknownHostException e) {
//            e.printStackTrace();
        } catch (SocketException e) {
//            e.printStackTrace();
        } catch (IOException e) {
//            e.printStackTrace();
        }
    }
}
