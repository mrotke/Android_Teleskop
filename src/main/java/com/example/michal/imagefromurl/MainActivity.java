package com.example.michal.imagefromurl;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;



import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    Timer timer;
    MyTimerTask myTimerTask;
    ImageView imageView;
    TextView textView;
    int i;
    {
        i = 0;
    }

    String Url = "http://192.168.1.116/pic.jpg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = (ImageView)findViewById(R.id.imageView);
        imageView.setOnTouchListener(imageViewTouchListener);
        textView = (TextView)findViewById(R.id.textView);
        timer = new Timer();
        myTimerTask = new MyTimerTask();
        timer.schedule(myTimerTask,10,100);


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    View.OnTouchListener imageViewTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN){
                UdpClientThread UDPthread = new UdpClientThread("X:"+String.valueOf(Math.round((320-event.getX())*3/2))+
                "Y:"+String.valueOf(Math.round(event.getY()*4/3)));
                UDPthread.start();
            }
            return true;
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();

        if (id == R.id.ConnSet){
            startActivity(new Intent(MainActivity.this,IPActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer = null;
        myTimerTask = null;

    }


    class MyTimerTask extends TimerTask
    {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new AsyncTaskLoadImage(imageView).execute("http://"+Model.dstAddress+"/pic.png");
                    i++;
                    //textView.setText(Integer.toString(i));
                    textView.setText(Integer.toString(imageView.getHeight())+":"+Integer.toString(imageView.getWidth()));
                }
            });
        }
    }



}
